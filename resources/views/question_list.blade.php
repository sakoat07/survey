@extends('layouts.app')

@section('content')
<div class="container">
  <div class="question_list_container">
    <form action="{{ url('/question/builder/Store')}}" id="survey_form" method="POST">
      {{ csrf_field() }}
    @if(isset($question_list) && !empty($question_list))
    <?php $i = 1;?>
    <div class="column">
    @foreach($question_list as $row)
    <div class="portlet">
      <div class="portlet-header">{{$i}}. {{$row->question_title}} <a class="icon_question_remove" question_id="{{$row->question_row_id}}"><img src="{{ asset('/') }}public/img/delete.png" style="vertical-align:middle;float:right;padding-right:7px;" width="16" height="16" /></a></div>
      <div class="portlet-content">
        <input type="hidden" name="question_id[]" value="{{$row->question_row_id}}" /> 
        @if($row->question_type == 2)
          <?php $option_list = json_decode($row->answer_option);?>
          @if($option_list)
            @foreach($option_list as $option)
            <div>{{$option}}</div>
            @endforeach
          @endif
        @elseif($row->question_type == 1)
          <div class="form-group">
            <label for="title">
                <input type="radio" name="option[]" value="0" class="form-control" />True
            </label>
            <label for="title">
                <input type="radio" name="option[]" value="0" class="form-control" />False
            </label>
          </div>
        @elseif($row->question_type == 3)
          <div class="form-group">
                <label for="text_response">Text Response:</label>
                <input type="text" name="text_response" class="form-control" id="text_response" />
          </div>
        @elseif($row->question_type == 4)
          <div class="form-group">
                <label for="numerical_response">Numerical Response:</label>
                <input type="text" name="numerical_response" class="form-control" id="numerical_response" />
          </div>
        @endif
      </div>
    </div>
    <?php $i++;?>
    @endforeach
  </div>
  @endif
  @if(isset($question_list) && count($question_list) > 0)
    <button type="submit" style="margin:10px;" class="right btn btn-primary">Update Survey</button>
  @endif
</form>
</div>
  
<div style="clear:both;"></div>
   <a class="question_add_menu">
        <img src="{{ asset('/') }}public/img/plus.png" style="vertical-align:middle;" width="16" height="16" /><span class="text-menu">QUESTION</span>
    </a>
    <div style="">
        <ul class="question_type_list">
            <li><a type="1" id="bolean_question">True or False</a></li>
            <li><a type="2" id="multiple_choice_question">Multiple Choice</a></li>
            <li><a type="3" id="text_response_question">Text Response</a></li>
            <li><a type="4" id="numerical_response_question">Numerical Response<a></li>
        </ul>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="multiplechoiceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Multiple Choice Question</h4>
      </div>
      <form action="{{ url('/question/store')}}" id="mcq_form" method="POST">
      <div class="modal-body">
            {{ csrf_field() }}
            <input type="hidden" name="type" value="2" />
            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" name="title" class="form-control" id="title" />
            </div>
            <input type="hidden" name="type" value="2">
            <div class="form-group">
                <label for="title">Option1:</label>
                <input type="text" name="option[]" class="form-control" />
            </div>
            <div class="form-group">
                <label for="title">Option2:</label>
                <input type="text" name="option[]" class="form-control" />
            </div>
            <div class="form-group">
                <label for="title">Option3:</label>
                <input type="text" name="option[]" class="form-control" />
            </div>
            <div class="form-group">
                <label for="title">Option4:</label>
                <input type="text" name="option[]" class="form-control" />
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save Question</button>
        </div>
        </form>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="boleanModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">True or False Question</h4>
      </div>
      <form action="{{ url('/question/store')}}" id="bolean_form" method="POST" />
          <div class="modal-body">
                {{ csrf_field() }}
                <input type="hidden" name="type" value="1" />
                <div class="form-group">
                    <label for="title">Title:</label>
                    <input type="text" name="title" class="form-control" id="title" />
                </div>
                <div class="form-group">
                    <label for="title">
                        <input type="radio" name="option[]" value="1" class="form-control" />True
                    </label>
                </div>
                <div class="form-group">
                    <label for="title">
                        <input type="radio" name="option[]" value="0" class="form-control" />False
                    </label>
                </div>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save Question</button>
          </div>
      </form>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="textModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Text Response Question</h4>
      </div>
      <form action="{{ url('/question/store')}}" id="text_form" method="POST">
        <div class="modal-body">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" name="title" class="form-control" id="title" />
            </div>
            <div class="form-group">
                <label for="text_response">Text Response:</label>
                <input type="text" name="text_response" class="form-control" id="text_response" />
            </div>
            <input type="hidden" name="type" value="3">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save Question</button>
        </div>
       </form>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="numericalModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Numerical Response Question</h4>
      </div>
      <form action="{{ url('/question/store')}}" id="numerical_form" method="POST">
      <div class="modal-body">
            {{ csrf_field() }}
            <input type="hidden" name="type" value="4" />
            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" name="title" class="form-control" id="title" />
            </div>
            <div class="form-group">
                <label for="numerical_response">Numerical Response:</label>
                <input type="text" name="numerical_response" class="form-control" id="numerical_response" />
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save Question</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection
@section('page_js')
<script type="text/javascript">
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(document).ready(function(){
        $('a#multiple_choice_question').on('click', function () {
            $('#multiplechoiceModal').modal('show');
        });
         $('a#bolean_question').on('click', function () {
            $('#boleanModal').modal('show');
        });
         $('a#text_response_question').on('click', function () {
            $('#textModal').modal('show');
        });
         $('a#numerical_response_question').on('click', function () {
            $('#numericalModal').modal('show');
        });
         $('#multiplechoiceModal').on('hidden.bs.modal', function (e) {
            $('#mcq_form').reset();
        });
          $('#bolean_form').on('hidden.bs.modal', function (e) {
            $('#mcq_form').reset();
        });
           $('#text_form').on('hidden.bs.modal', function (e) {
            $('#mcq_form').reset();
        });
            $('#numerical_form').on('hidden.bs.modal', function (e) {
            $('#mcq_form').reset();
        });

        $('a.icon_question_remove').on('click', function () {
           var question_id = $(this).attr('question_id');
           var curr_link = $(this);
           $.ajax({
            url: "{{ url('/question/destroy')}}",
            type: 'GET',
            dataType:"HTML",
            data: {
                question_row_id:question_id
              },
            success: function (data)
            {
              $(curr_link).parent().parent().remove();
            }
          });
        });

       $( ".column" ).sortable({
          connectWith: ".column",
          handle: ".portlet-header",
          cancel: ".portlet-toggle",
          placeholder: "portlet-placeholder ui-corner-all"
        });
 
      $(".portlet").addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
      .find( ".portlet-header" ).addClass( "ui-widget-header ui-corner-all" )
        .prepend( "<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");
    $( ".portlet-toggle" ).on( "click", function() {
      var icon = $( this );
      icon.toggleClass( "ui-icon-minusthick ui-icon-plusthick" );
      icon.closest( ".portlet" ).find( ".portlet-content" ).toggle();
    });
  });
</script>
@endsection
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/
//Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::post('/question/store', 'QuestionController@store');
Route::get('/question/list', 'QuestionController@index');
Route::post('/question/builder/Store', 'QuestionController@storeBuilder');
Route::get('/question/destroy/', 'QuestionController@deleteQuestion');
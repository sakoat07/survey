<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $table = 'survey';
    public $timestamps = false;
    protected $primaryKey = 'survey_row_id';
}

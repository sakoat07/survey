<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Question;
use App\Survey;
use Illuminate\Support\Facades\Input;

class QuestionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   $question_list =  \App\Question::select('*')->orderBy('sort_order')->get();
        return view('question_list', compact('question_list'));
    }

    public function store(Request $request){
        if ($request->isMethod('post')) {
            $type = $request->type;
            $Question = new Question();
            $Question->question_title = $request->title;
            $Question->question_type = $request->type;
            if($type == 1){
                $Question->answer_option = json_encode(['1', '0']);
            } elseif($type == 3 || $type == 4){
                $Question->answer_option = '';
            } else {
                $Question->answer_option = json_encode($request->option);
            }
            $Question->save();
        }
        return redirect('/question/list');
    }
    public function storeBuilder(Request $request){
        if ($request->isMethod('post')) {
            $question_id_list = $request->question_id;
            //dd($question_id_list);
            if($question_id_list){
                $i = 1;
                foreach($question_id_list as $key => $question_id){
                    $Question = \App\Question::where('question_row_id', $question_id)->first();
                    $Question->sort_order = $i;
                    $Question->save();
                    $i++;
                }
            }
        }
        return redirect('/question/list');
    }

    public function deleteQuestion(){
        $question_row_id = Input::get('question_row_id');
        $Question = \App\Question::find($question_row_id);
        $Question->delete();
        echo 1;    
    }
}
